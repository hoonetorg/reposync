#!/usr/bin/env python3
import sys
import os
import shutil
import logging
import pprint
import argparse
import subprocess

import pathlib

import yaml
import jinja2
import deepmerge
import re

import requests
import tarfile
import gzip

import pycdlib




scriptName = sys.argv[0]
scriptBasename = os.path.basename(scriptName)
scriptDirname = os.path.dirname(os.path.abspath(scriptName))

logging.basicConfig(level=logging.NOTSET,
                    format = '%(asctime)s.%(msecs)03d %(levelname)s %(module)s '
                             '%(funcName)s: %(message)s',
                    datefmt = '%Y-%m-%d %H:%M:%S')
log=logging.getLogger()

class Repo(object):
    def __init__(self,
            configFiles):
        self.configFiles = configFiles

    @staticmethod
    def firstFound(fs):
        log.debug('fs: {}'.format(pprint.pformat(fs)))
        for f in fs:
            if pathlib.Path(f).is_file():
                return f
        return None

    @staticmethod
    def download(url, outFileName, overwrite = False):
        log.info('dowloading {} to {}'.format(url, outFileName))
        if not os.path.isfile(outFileName) or overwrite:
            with requests.get(url, stream=True) as r:
                r.raise_for_status()
                with open(outFileName, 'wb') as f:
                    for chunk in r.iter_content(chunk_size=1048576): 
                        f.write(chunk)

    @staticmethod
    def extractTarFileTo(archiveName, archiveFileName, outFileName, openMode = 'r:gz'):
        log.info('extract {} from {} to {}'.format(archiveFileName, archiveName, outFileName))
        archive = tarfile.open(archiveName, openMode)
        archiveFile = archive.extractfile(archiveFileName)
        outFile = open(outFileName, 'wb')
        outFile.write(archiveFile.read())
        archiveFile.close()
        archive.close()
        outFile.close()

    @staticmethod
    def createInitrdWithFirmware(initrdName, firmwareName, backup = False):
        if backup:
            if isinstance(backup, str):
                ext = backup
            else:
                ext = '.orig'
            log.info('backing up {} to {}'.format(initrdName, initrdName + ext))
            shutil.copy(initrdName, initrdName + ext)

        log.info('creating new {} containing {}'.format(initrdName, firmwareName))
        with open(initrdName, 'ab') as initrd, open(firmwareName, 'rb') as firmware:
            initrd.write(firmware.read())

    @staticmethod
    def listFileFromIso(isoFile, startPath, suffix):
        log.info('isoFile: {}'.format(isoFile))
        iso = pycdlib.PyCdlib()
        iso.open(isoFile)
        if iso.has_udf():
            pathType = 'udf_path'
        elif iso.has_rock_ridge():
            pathType = 'rr_path'
        elif iso.has_joliet():
            pathType = 'joliet_path'
        else:
            pathType = 'iso_path'
        log.info("Using path type of '{}'".format(pathType))

        fileListSuffix = []
        for dirName, dirList, fileList in iso.walk(**{pathType: startPath}):
            for fileName in fileList:
                if fileName.endswith(suffix):
                    #log.debug('filename: {}'.format(dirName + '/' + fileName))
                    fileListSuffix.append(dirName + '/' + fileName)
        return fileListSuffix

        iso.close()


    @staticmethod
    def extractFilesFromIso(isoFile, extractFiles):
        log.info('isoFile: {}'.format(isoFile))
        iso = pycdlib.PyCdlib()
        iso.open(isoFile)
        if iso.has_udf():
            pathType = 'udf_path'
        elif iso.has_rock_ridge():
            pathType = 'rr_path'
        elif iso.has_joliet():
            pathType = 'joliet_path'
        else:
            pathType = 'iso_path'
        log.info("Using path type of '{}'".format(pathType))

        for extractFile in extractFiles:
            log.debug('extracting {} to {}'.format(extractFile[1], extractFile[0]))
            os.makedirs(name = os.path.dirname(extractFile[0]), mode = 0o755, exist_ok=True)
            iso.get_file_from_iso(extractFile[0], **{pathType: extractFile[1]})
        iso.close()
        
    @staticmethod
    def syncFiles(source, target, suffix):
        log.info('source: {}, target {}, suffix {}'.format(source, target, suffix))
        fileListSuffix = []
        for dirName, dirList, fileList in os.walk(source, topdown=False):
            relDirName = os.path.relpath(dirName, source)
            targetDirName = os.path.join(target, relDirName)
            for fileName in fileList:
                if fileName.endswith(suffix):
                    log.debug('copying {} to {}'.format(os.path.join(dirName, fileName), os.path.join(targetDirName, fileName)))
                    os.makedirs(name = targetDirName, mode = 0o755, exist_ok=True)
                    shutil.copy(os.path.join(dirName, fileName), os.path.join(targetDirName, fileName))

    @staticmethod
    def createDistFiles(repoDir, relPackageDir, codename, component, architecture, version, suite = 'stable' ):
        relDistPackageDir = repoDir + '/dists/' + codename +'/' + component + '/binary-' + architecture 
        packageFileName = relDistPackageDir + '/Packages'
        with open(packageFileName, 'w') as packageFile:
            subprocess.run([
                           'apt-ftparchive',
                           '--arch', architecture,
                           'packages',
                           relPackageDir,
                           ],
                           cwd = repoDir, 
                           stdout = packageFile,
                          )
        with open(packageFileName, 'rb') as packagesFile, gzip.open(packageFileName + '.gz', 'wb') as gzippedPackagesFile:
            gzippedPackagesFile.write(packagesFile.read())
    
        relDistContentDir = repoDir + '/dists/' + codename +'/' + component  
        contentsFileName = relDistContentDir + '/Contents-' + architecture
        with open(contentsFileName, 'w') as contentFile:
            subprocess.run([
                           'apt-ftparchive',
                           'contents',
                           relPackageDir,
                           ],
                           cwd = repoDir, 
                           stdout = contentFile,
                          )
        with open(contentsFileName, 'rb') as contentsFile, gzip.open(contentsFileName + '.gz', 'wb') as gzippedPackagesFile:
            gzippedPackagesFile.write(contentsFile.read())
    
        releaseFileName = relDistPackageDir + '/Release'
        releaseTemplateData = '''Archive: {{ suite }}
Origin: Debian
Label: Debian
Version: {{ version }} 
Component:  {{ component }}
Architecture: {{ architecture }}
'''
        releaseTemplate = jinja2.Template(releaseTemplateData)
        releaseTemplate.stream(
                suite = suite, 
                version = version, 
                codename = codename, 
                component = component, 
                architecture = architecture
                ).dump(releaseFileName)

    @staticmethod
    def createGlobalDistFiles(repoDir, codename, components, architectures, version, suite = 'stable' ):
        relDistDir = 'dists/' + codename 
        releaseFileName = repoDir + '/' + relDistDir + '/Release'
        releaseConfigFileName = repoDir + '/release_' + codename + '.conf'

        releaseTemplateData = '''APT::FTPArchive::Release::Origin "Debian";
APT::FTPArchive::Release::Label "Debian";
APT::FTPArchive::Release::Codename "{{ codename }}";
APT::FTPArchive::Release::Suite "{{ suite }}";
APT::FTPArchive::Release::Version "{{ version }}";
APT::FTPArchive::Release::Components "{{ ' '.join(components) }}";
APT::FTPArchive::Release::Architectures "{{ ' '.join(architectures) }}";
APT::FTPArchive::Release::Description "Debian {{ suite }} ({{ codename }}), version {{ version }}";

'''
        releaseTemplate = jinja2.Template(releaseTemplateData)
        releaseTemplate.stream(
                suite = suite, 
                version = version, 
                codename = codename, 
                components = components, 
                architectures = architectures
                ).dump(releaseConfigFileName)

        try:
            with open(releaseFileName, 'w') as releaseFile:
                subprocess.run([
                           'apt-ftparchive',
                           'release',
                           '-c=' + releaseConfigFileName,
                           relDistDir,
                           ],
                           cwd = repoDir, 
                           stdout = releaseFile,
                          )
        except Exception as e:
            sys.exit('could not create Release file with error: \n{}\n\n'.format( pprint.pformat(str(e)) ) )


    def getCodenameConfig(self, codename, **kwargs):
        defaultConfig = self.getRepoConfig(**kwargs).get('default', {})
        codenameConfig = self.getRepoConfig(**kwargs).get('codenames', {}).get(codename, {})
        self._mergeConfig(defaultConfig, codenameConfig, **kwargs)
        return defaultConfig

    def getRepoConfig(self, **kwargs):
        return self._getConfigFromFile(self.firstFound(self.configFiles))

    def _getConfigFromFile(self, configFile):
        log.debug('configFile: {}'.format(configFile))
        try:
            with open(configFile, "r") as f:
                return yaml.safe_load(f)
        except IOError as e:
            errno, strerror = e.args
            sys.exit('configFile {}: I/O error({}): \n{}\n\n'.format( configFile, errno, pprint.pformat(strerror) ) )
        except yaml.YAMLError as e:
            sys.exit('configFile {}: Invalid YAML: \n{}\n\n'.format( configFile, pprint.pformat(str(e)) ) )

        except Exception as e:
            sys.exit('configFile {}: could not be read with error: \n{}\n\n'.format( configFile, pprint.pformat(str(e)) ) )

    def _mergeConfig(self, *configs, **kwargs):
        # possible values: append override prepend
        listStrategy = kwargs.get('listStrategy', 'append')
        # possible values: merge override
        dictStrategy = kwargs.get('dictStrategy', 'merge')
        # possible values: override use_existing
        fallbackStrategy = kwargs.get('fallbackStrategy', 'override')
        # possible values: override override_if_not_empty use_existing
        conflictStrategy = kwargs.get('conflictStrategy', 'override')
         
        log.debug('configs\n{}'.format( pprint.pformat(configs) ))

        configMerger = deepmerge.Merger(
            [
                (list, [listStrategy]),
                (dict, [dictStrategy])
            ],
            [fallbackStrategy],
            [conflictStrategy]
        )
        config0 = configs[0]
        log.debug('config0:\n{}\n\n'.format( pprint.pformat(config0) ))
        for config in configs[1:]:
            configMerger.merge( config0, config )
            log.debug('config:\n{}\n\n'.format( pprint.pformat(config) ))
            log.debug('merged config0: \n{}\n\n'.format( pprint.pformat(config0) ))



def main():
    global scriptName
    global scriptBasename
    global scriptDirname
    log.debug('scriptName: \"{}\"'.format(scriptName))
    log.debug('scriptBasename: \"{}\"'.format(scriptBasename))
    log.debug('scriptDirname: \"{}\"'.format(scriptDirname))

    parser = argparse.ArgumentParser(prog = scriptBasename)
    parser.add_argument('--configFile',
                        default = '/etc/' + scriptBasename + '/' + scriptBasename + '.yml',
                        dest =  'configFile',
                        help = ('the configFile'),
                       )
    parser.add_argument('--repoDir',
                        default = '/reposync/debian/debinst',
                        dest =  'repoDir',
                        help = ('the repoDir'),
                       )
    args = parser.parse_args()

    configFile = args.configFile
    repoDir = args.repoDir

    configFiles = ( scriptDirname + '/' + scriptBasename + '.yml', configFile )

    repo = Repo(configFiles = configFiles)

    log.debug('repo config:\n{}\n'.format(pprint.pformat(repo.getRepoConfig())))
    log.debug('repo dir: {}'.format(repoDir))

    os.makedirs(name = repoDir, mode = 0o755, exist_ok=True)

    for codename in repo.getRepoConfig().get('codenames', {}).keys():
        log.debug('codename: {}'.format(codename))

            
        codenameConfig = repo.getCodenameConfig(codename = codename)
        log.debug('codenameConfig:\n{}\n'.format(pprint.pformat(codenameConfig)))
   
        suite                 = codenameConfig.get('suite')
        version               = codenameConfig.get('version')
        curVer                = codenameConfig.get('curVer')
        architectures         = codenameConfig.get('architectures')
        components            = codenameConfig.get('components')

        pkgMirrorUdebProtocol = codenameConfig.get('pkgMirrorUdebProtocol') or codenameConfig.get('pkgMirrorProtocol')
        pkgMirrorUdebHost     = codenameConfig.get('pkgMirrorUdebHost') or codenameConfig.get('pkgMirrorHost')
        pkgMirrorUdebRepoDir  = codenameConfig.get('pkgMirrorUdebRepoDir') or codenameConfig.get('pkgMirrorRepoDir')

        pkgMirror             = codenameConfig.get('pkgMirrorProtocol') + '://' + codenameConfig.get('pkgMirrorHost') + '/' + codenameConfig.get('pkgMirrorRepoDir')
        pkgMirrorUdeb         = pkgMirrorUdebProtocol                   + '://' + pkgMirrorUdebHost                   + '/' + pkgMirrorUdebRepoDir

        netbootArchive        = codenameConfig.get('netbootArchive')
        netbootCsumAlgo       = codenameConfig.get('netbootCsumAlgo')

        cdMirror              = codenameConfig.get('cdMirror')
        cdPath                = codenameConfig.get('cdPath')

        firmwareArchive       = codenameConfig.get('firmwareArchive')
        firmwareCsumAlgo      = codenameConfig.get('firmwareCsumAlgo')

        isoPath               = codenameConfig.get('isoPath')
        isoCsumAlgo           = codenameConfig.get('isoCsumAlgo')
        isoType               = codenameConfig.get('isoType')

        for architecture in architectures:
            log.debug('architecture: {}'.format(architecture))

            distPathImages = 'main/installer-' + architecture + '/current/images'
            iso = codenameConfig.get('iso').get(architecture)

            # linux + initrd
            print('commands for codename {}:'.format(codename))

            downloadDirArchives = repoDir + '/' + codename + '/' + architecture
            os.makedirs(name = downloadDirArchives, mode = 0o755, exist_ok=True)

            # download netboot
            repo.download(url = pkgMirrorUdeb + '/dists/' + codename + '/' + distPathImages + '/netboot/' + netbootArchive, outFileName = downloadDirArchives + '/' + netbootArchive)
            repo.download(url = pkgMirrorUdeb + '/dists/' + codename + '/' + distPathImages + '/' + netbootCsumAlgo.upper() + 'SUMS', outFileName = downloadDirArchives + '/' + netbootArchive + '.' + netbootCsumAlgo)
            # FIXME check checkusm
            
            # download firmware
            repo.download(url = cdMirror + '/' + cdPath + '/firmware/' + codename + '/current/' + firmwareArchive, outFileName = downloadDirArchives + '/' + firmwareArchive)
            repo.download(url = cdMirror + '/' + cdPath + '/firmware/' + codename + '/current/' + firmwareCsumAlgo.upper() + 'SUMS', outFileName = downloadDirArchives + '/' + firmwareArchive + '.' + firmwareCsumAlgo)
            # FIXME check checkusm
            
            # download large iso
            repo.download(url = cdMirror + '/' + cdPath + '/cd-including-firmware/' + isoPath + '/' + architecture + '/iso-' + isoType + '/' + iso, outFileName = downloadDirArchives + '/' + iso)
            repo.download(url = cdMirror + '/' + cdPath + '/cd-including-firmware/' + isoPath + '/' + architecture + '/iso-' + isoType + '/' + isoCsumAlgo.upper() + 'SUMS', outFileName = downloadDirArchives + '/' + iso + '.' + isoCsumAlgo)
            ##FIXME check checkusm

            #extract linux/initrd
            repo.extractTarFileTo(archiveName = downloadDirArchives + '/' + netbootArchive, archiveFileName = './debian-installer/' + architecture + '/linux', outFileName = downloadDirArchives + '/' + 'linux')
            repo.extractTarFileTo(archiveName = downloadDirArchives + '/' + netbootArchive, archiveFileName = './debian-installer/' + architecture + '/initrd.gz', outFileName = downloadDirArchives + '/' + 'initrd.gz')
             
            repo.createInitrdWithFirmware(initrdName = downloadDirArchives + '/' + 'initrd.gz', firmwareName = downloadDirArchives + '/' + firmwareArchive, backup = True)

            # create repo
            for component in components: 
                subprocess.run([
                               '/usr/bin/debmirror',
                               '--diff=none',
                               '-p',
                               '-v',
                               '--no-check-gpg',
                               '--ignore-release-gpg',
                               '--nosource',
                               '--rsync-extra=none',
                               '-e', pkgMirrorUdebProtocol,
                               '-h', pkgMirrorUdebHost, 
                               '-r', pkgMirrorUdebRepoDir,
                               '-a', architecture,
                               '-d', codename,
                               '-s', component + '/debian-installer',
                               repoDir + '/' + codename + '/' + architecture + '/udebs/', 
                               ])

                os.makedirs(name = repoDir + '/pool-' + codename + '-' + architecture + '/' + component, mode = 0o755, exist_ok=True)
                os.makedirs(name = repoDir + '/udebpool-' + codename + '-' + architecture + '/' + component, mode = 0o755, exist_ok=True)

                repo.syncFiles(source = repoDir + '/' + codename + '/' + architecture + '/udebs/pool', target = repoDir + '/pool-' + codename + '-' + architecture, suffix = '.deb')
                repo.syncFiles(source = repoDir + '/' + codename + '/' + architecture + '/udebs/pool', target = repoDir + '/udebpool-' + codename + '-' + architecture, suffix = '.udeb')


            debFileListIn = repo.listFileFromIso(isoFile = downloadDirArchives + '/' + iso, startPath = '/pool', suffix = '.deb')
            #log.debug('debFileListIn:\n{}\n'.format(pprint.pformat(debFileListIn)))
            udebFileListIn = repo.listFileFromIso(isoFile = downloadDirArchives + '/' + iso, startPath = '/pool', suffix = '.udeb')
            #log.debug('udebFileListIn:\n{}\n'.format(pprint.pformat(udebFileListIn)))

            regexDeb = re.compile(r'^/pool/')
            debFileOut = []
            for debFile in debFileListIn:
                debFileArch = regexDeb.sub('/pool-' + codename + '-' + architecture + '/', debFile)
                log.debug('debFile {} debFileArch {}'.format(debFile, debFileArch))
                debFileOut.append([ repoDir + debFileArch, debFile ]) 
            repo.extractFilesFromIso(isoFile = downloadDirArchives + '/' + iso, extractFiles = debFileOut)
            udebFileOut = []
            for udebFile in udebFileListIn:
                udebFileArch = regexDeb.sub('/udebpool-' + codename + '-' + architecture + '/', udebFile)
                log.debug('udebFile {} udebFileArch {}'.format(udebFile, udebFileArch))
                udebFileOut.append([ repoDir + udebFileArch, udebFile ]) 
            repo.extractFilesFromIso(isoFile = downloadDirArchives + '/' + iso, extractFiles = udebFileOut)

        for architecture in architectures + [ 'all' ]:
            for component in components: 
            
                os.makedirs(name = repoDir + '/dists/' + codename + '/' + component + '/binary-' + architecture, mode = 0o755, exist_ok=True)
                os.makedirs(name = repoDir + '/dists/' + codename + '/' + component + '/debian-installer/binary-' + architecture, mode = 0o755, exist_ok=True)
            
                relPackageDirDeb = 'pool-' + codename + '-' + architecture + '/' + component
                repo.createDistFiles(repoDir = repoDir, relPackageDir = relPackageDirDeb, codename = codename, component = component, architecture = architecture, version = version, suite = suite)

                relPackageDirUdeb = 'udebpool-' + codename + '-' + architecture + '/' + component
                repo.createDistFiles(repoDir = repoDir, relPackageDir = relPackageDirUdeb, codename = codename, component = component + '/debian-installer', architecture = architecture, version = version, suite = suite)

        componentsWithInstaller = []
        for component in components: 
            componentsWithInstaller.append(component)
            componentsWithInstaller.append(component + '/debian-installer')

        log.debug('componentsWithInstaller:\n{}\n'.format(pprint.pformat(componentsWithInstaller)))

        repo.createGlobalDistFiles(repoDir = repoDir, codename = codename, components = componentsWithInstaller, architectures = architectures, version = version, suite = suite)
            
        #FIXME: gpg signing
        ## Replace $(id -un) with your desired gpg signing key    
        ##'gpg -a --yes --output dists/' + codename + '/Release.gpg --local-user $(id -un) --detach-sign dists/' + codename + '/Release'
        ##'gpg -a --yes --clearsign --output dists/' + codename+ '/InRelease --local-user $(id -un) --detach-sign dists/' + codename + '/Release'
    
    
if __name__ == '__main__':
    sys.exit(main())
    
